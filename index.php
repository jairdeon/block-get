<?php include('template/header.php'); ?>
<div class="container">

<div class="row">
    <form class="col s12">
      <div class="row">
        
        <div class="input-field col s12">
        <div>
          <h4 class="userName">Olá, Visitante!</h4>
          <p>Quero conhecer mais sobre você.</p>
        </div>
        </div>

        <div class="input-field col s12">
          <input name="name" placeholder="Digite seu nome aqui." id="userName" value="" type="text">
          <label for="userName">Qual é o seu nome?</label>
        </div>

        <div class="input-field col s12">
        <button type="submit" class="waves-effect waves-light btn indigo"><i class="material-icons left">send</i>Enviar</button>
        </div>
        
      </div>
    </form>
    
    <div class="col s12">
      <p>O propósito deste trabalho é criar um formulário e validar se é em formato POST, mas aqui o AJAX já faz isso :)
        <br>
        <small>Não tente burlar colocando o GET manualmente, já pensei nisso ;)</small>
      </p>      
    </div>
  </div>


</div>
<?php include('template/footer.php'); ?>