var url_string = window.location.href; //window.location.href
var url = new URL(url_string);

if(url.search.length > 0) {
    $.alert({
            title: 'Epa, está tentando usar GET manualmente, né?',
            content: 'Neste projeto não pode, senão eu perco ponto :(',
            buttons: {
                post: {
                    text: 'Ok',
                    action: function () {                                
                        redirectUser();
                    }
                }                        
            }
        });
}

$('input#userName').change(function() {
    $('h4.userName').html('Olá, ' + $(this).val() + '!');
});
$('form').submit(function(e) {
    let userName = $('input#userName').val();
    if(userName.length <= 0) {
        $.alert({
            title: 'Ei, visitante.',
            content: 'você precisa primeiro me dizer seu nome!',
            buttons: {
                ok: {
                    text: 'Ok, vou colocar'
                }
            }
        });                
    } else {

        $.alert({
            title: userName + ', como você quer mandar essa requisição?',
            content: 'Selecione se é via POST ou GET',
            buttons: {
                post: {
                    text: 'POST',
                    action: function () {
                        $.post('template/validate.php', {name : userName}, function(data) {
                            $.alert({
                                title: 'Olá, ' + data.name + ':',
                                content: 'Bem vindo ao: ' + data.serverName + ', este servidor está rodando no IP ' + data.serverIP + ' e na porta ' + data.serverPort + '<br>Você acessou a URL: ' + data.currentUrl + ' usando o método ' + data.method + ' pelo IP ' + data.clientIP + '.',
                                buttons: {
                                    two: {
                                        text: 'E se fosse via GET?',
                                        action: function() {
                                            sendGET()
                                        }
                                    }
                                }
                            });                                    
                        });
                    }
                },
                get: {
                    text: 'GET',                                    
                    action: function () {
                         sendGET();
                    }
                },
            }
        });
        
    }

    e.preventDefault;
    return false;
});

function redirectUser() {
    window.location = location.protocol + '//' + location.host + location.pathname;
}

function sendGET() {
    let userName = $('input#userName').val();
    $.get('template/validate.php', {name : userName}, function(data) {
        if(data.error) {
            $.alert({
                title: 'Olá, ' + data.name + '. Você usou o método GET!',
                content: 'Bem vindo ao: ' + data.serverName + ', este servidor está rodando no IP ' + data.serverIP + ' e na porta ' + data.serverPort + '<br>Você acessou a URL: ' + data.currentUrl + ' usando o método ' + data.method + ' pelo IP ' + data.clientIP + '.<br><br>Ah.. mas aqui, o trabalho não permite que o formulário aceite o modo GET, então eu vou redirecionar você, ok?',
                buttons: {
                    one: {
                        text: 'Você está mentindo!',
                        action: function () {   
                            let getData = '';
                            $.each(data.get, function(param, value) {
                                getData += 'Parâmetro: ' + param + ' | Valor: ' + value + '<br>';
                            });                                                        
                            $.alert({
                                title: 'Vou te provar.',
                                content: 'Olhe o arquivo js/app.js, na linha 83 nós usamos a função $.get do Jquery. No arquivo validate.php na linha 23 e 33, utilizamos o $_GET, e existe uma verificação com o navegador verificando se a requisição foi via GET ou POST.<br><br>Os dados GET que você enviou foram:<br>' + getData + '<br>Ah, não adianta colocar parametros manualmente, o site não permite ;)',
                                buttons: {
                                    ok: {
                                        text: 'Tudo bem, você está certo',
                                        action: function() {
                                            redirectUser();
                                        }
                                    }
                                }
                            });

                        }
                    },
                    two: {
                        text: 'Ok, pode me redirecionar.',
                        action: function () {
                            redirectUser();
                        }
                    },
                }
            });
        }
    });  
}