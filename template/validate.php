<?php
header('Content-Type: application/json');
$serverMethod = $_SERVER['REQUEST_METHOD'];
$serverName = $_SERVER['SERVER_NAME'];
$serverIP = $_SERVER['SERVER_ADDR'];
$serverPort = $_SERVER['SERVER_PORT'];
$currentUrl = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$clientIP = $_SERVER['REMOTE_ADDR'];

if($serverMethod === 'POST') {
    $userName = $_POST['name'];
    $result = array(
        'error' => false,
        'name' => $userName,
        'method' => $serverMethod,
        'serverName' => $serverName,
        'serverIP' => $serverIP,
        'serverPort' => $serverPort,
        'currentUrl' => $currentUrl,
        'clientIP' => $clientIP
    ); echo json_encode($result);
} else {        
    $userName = $_GET['name'];
    $result = array(
        'error' => true,
        'name' => $userName,
        'method' => $serverMethod,
        'serverName' => $serverName,
        'serverIP' => $serverIP,
        'serverPort' => $serverPort,
        'currentUrl' => $currentUrl,
        'clientIP' => $clientIP,
        'get' => $_GET
    ); echo json_encode($result);
}