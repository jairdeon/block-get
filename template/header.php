<!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="node_modules/jquery-confirm/dist/jquery-confirm.min.css" />

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Block GET: Jair Deon</title>

      <meta property="og:type" content="article">
      <meta property="og:title" content="Block GET">
      <meta property="og:site_name" content="Jair Deon">
      <meta property="og:description" content="Uma pequena aplicação que utiliza AJAX para submissão de formulários onde detecta se é do tipo GET ou POST.">
      <meta property="og:image" content="https://www.jairdeon.com/plugins/block-get/images/fb.jpg">
      <meta property="og:image:type" content="image/jpeg">
      <meta property="og:image:width" content="800">
      <meta property="og:image:height" content="400">
      <meta property="article:author" content="Jair Deon">
      <meta property="article:section" content="Tutoriais">
      <meta property="article:tag" content="PHP, JavaScript, Materialize, Jquery">

    </head>
    <body>